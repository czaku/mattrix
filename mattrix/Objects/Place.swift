
import Foundation

class Place : NSObject {
    
    var id: String?
    var name: String?
    var rating: Float?
    var photoReference: String?
    
}