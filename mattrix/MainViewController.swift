

import UIKit
import CoreLocation

class MainViewController: UIViewController, CLLocationManagerDelegate, GameViewControllerDelegate, UIAlertViewDelegate {
    
    @IBOutlet weak var attemptsLabel: UILabel!
    @IBOutlet weak var bestScoreLabel: UILabel!
    
    var locationManager: CLLocationManager?
    var location: CLLocation?
    
    var gameViewController: GameViewController?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        requestLocation()
    
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "GameViewController",
            let viewController = segue.destinationViewController as? GameViewController {
            
            gameViewController = viewController
                
            gameViewController?.delegate = self
            
        }
        
    }
    
    //MARK: Location methods
    
    func requestLocation() {
        
        locationManager = CLLocationManager()
        
        locationManager?.delegate = self
        
        locationManager?.requestWhenInUseAuthorization()
        
        locationManager?.startUpdatingLocation()
        
        showWaitOverlay()
        
    }
    
    func processLocationResult(location:CLLocation) {
        
        self.location = location
        
        NetworkManager.sharedInstance.retrievePlacesForLocation(location) { places in
            
            if let places = places as? [Place] {
                
                DataStore.sharedInstance.updatePlaces(places)
                                
            }
            
            self.removeAllOverlays()
            
        }
        
    }
    
    //MARK: CLLocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
        if let location = locations.last where locations.count > 0 {
            
            processLocationResult(location)
            
            locationManager?.stopUpdatingLocation()
            
        }
        
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        if status != .AuthorizedWhenInUse {
        
            useFallbackLocation()
            
        }
        
    }
    
    func useFallbackLocation() {
        
        if let latitude = Double(Consts.fallbackLatitude.rawValue),
            let longitude = Double(Consts.fallbackLongitude.rawValue) {
                
                let location = CLLocation(latitude: latitude, longitude: longitude)
                
                processLocationResult(location)
                
                locationManager?.stopUpdatingLocation()
                
        }
        
    }
    
    //MARK: Actions
    
    @IBAction func startOverButtonPressed(sender:AnyObject) {
        
        DataStore.sharedInstance.clearCurrentGame()
        
        gameViewController?.loadGame()
        
        updateAttempts()
        
    }
    
    @IBAction func reloadButtonPressed(sender:AnyObject) {
        
        showWaitOverlay()
        
        DataStore.sharedInstance.clearDataStore()
        
        gameViewController?.loadGame()
        
        updateAttempts()
        
        if CLLocationManager.authorizationStatus() != .AuthorizedWhenInUse {
            
            useFallbackLocation()
            
        } else {
            
            requestLocation()
            
        }
        
    }
    
    //MARK: GameViewControllerDelegate methods
    
    func updateAttempts() {
        
        attemptsLabel.text = NSLocalizedString("ATTEMPTS: ", comment: "") + "\(DataStore.sharedInstance.attempts)"
        
    }
    
    func gameSuccess() {
        
        let currentScore = DataStore.sharedInstance.attempts
        
        let bestScore = NSUserDefaults.standardUserDefaults().valueForKey(Consts.UserDefaultsBestScore.rawValue)
        
        let alertView = UIAlertView(title: "Congratulations!", message: "You won the game! It took you \(currentScore) attempts.", delegate: self, cancelButtonTitle: "Dismiss")
        
        alertView.show()
        
        if let bestScore = bestScore as? Int where bestScore < currentScore {
            
            bestScoreLabel.text = NSLocalizedString("BEST SCORE: ", comment: "") + "\(bestScore)"
            
        } else {
            
            NSUserDefaults.standardUserDefaults().setValue(currentScore, forKey: Consts.UserDefaultsBestScore.rawValue)
            
            bestScoreLabel.text = NSLocalizedString("BEST SCORE: ", comment: "") + "\(currentScore)"
            
        }
        
    }

}

