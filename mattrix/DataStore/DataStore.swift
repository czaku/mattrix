
import Foundation

class DataStore : NSObject {
    
    static let sharedInstance = DataStore()
    
    var places = [Place]()
    var allocatedPlaces = [Place]()
    var currentTaps = [GameCell]()
    var guessedPlaces = [Place]()
    
    var attempts: Int = 0
    
    override init() {
        
        attempts = 0
        
        places = []
        
    }
    
    func updatePlaces(var array:[Place]) {

        array.sortInPlace({ $0.rating > $1.rating })
        
        places = Array(array.prefix(8))
        
    }
    
    func allocateGamePlaces() {
        
        allocatedPlaces.removeAll()
        
        allocatedPlaces.appendContentsOf(places)
        
        allocatedPlaces.appendContentsOf(places)
        
        allocatedPlaces.shuffle()
        
    }
    
    func registerTapWithResult(cell:GameCell) -> Bool {
        
        if guessedPlaces.contains(cell.place!) {
            
            return false
            
        }
        
        if !currentTaps.contains(cell) {
            
            currentTaps.append(cell)
            
            if currentTaps.count == 3 {
                
                currentTaps.removeFirst()
                
                currentTaps.removeFirst()
                
            }
            
            cell.setup()
            
            if currentTaps.count == 2 {
                
                let firstTapCell = currentTaps[0]
                
                let secondTapCell = currentTaps[1]
                
                attempts++
                
                if firstTapCell.place!.id == secondTapCell.place!.id {
                    
                    print("MATCHED - \(firstTapCell.place!.name)")
                    
                    guessedPlaces.append(firstTapCell.place!)
                    
                    firstTapCell.setup()
                    
                    secondTapCell.setup()
                    
                    firstTapCell.imageFadeIn(false)
                    
                    secondTapCell.imageFadeIn(false)

                    if guessedPlaces.count >= places.count {
                        
                        print("WON")
                        
                        return true
                        
                    }
                    
                    currentTaps.removeAll()
                    
                    return false
                    
                }
                
                currentTaps.removeAll()
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
                    
                    firstTapCell.imageFadeIn(false)
                    
                    secondTapCell.imageFadeIn(false)
                    
                    print("NOT MATCHED = \(firstTapCell.place!.name)")
                
                }
                
            }
            
        }
        
        return false
        
    }
    
    func isCurrentlyTapped(cell:GameCell) -> Bool {
        
        if currentTaps.contains(cell) {
            
            return true
            
        }
        
        return false
        
    }
    
    func isPlaceGuessed(place:Place) -> Bool {
        
        if guessedPlaces.contains(place) {
            
            return true
            
        }
        
        return false
        
    }
    
    func clearCurrentGame() {
        
        allocatedPlaces.removeAll()
        
        currentTaps.removeAll()
        
        guessedPlaces.removeAll()
        
        attempts = 0
        
    }
    
    func clearDataStore() {
        
        clearCurrentGame()
        
        places.removeAll()
        
    }
    
}