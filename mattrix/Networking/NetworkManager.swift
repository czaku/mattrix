
import Foundation
import Alamofire
import AlamofireImage
import CoreLocation

class NetworkManager : NSObject {
    
    static let sharedInstance = NetworkManager()
    
    let imageCache = AutoPurgingImageCache(memoryCapacity: 100 * 1024 * 1024, preferredMemoryUsageAfterPurge: 60 * 1024 * 1024)
    
    func retrievePlacesForLocation(location:CLLocation, completion:ArrayCompletion) {
        
        let params = ["location": "\(location.coordinate.latitude),\(location.coordinate.longitude)", "radius": "1000", "types": "restaurant", "key": Consts.GooglePlacesAPIKey.rawValue]
        
        Alamofire.request(.GET, Consts.GooglePlacesLocationBaseUrl.rawValue, parameters:params)
            .responseJSON { response in
                
            if let json = response.result.value,
                let array = json["results"] as? [AnyObject] {
                
                    self.parsePlacesResponse(array, completion: completion)
                
            }
                
        }
        
    }
    
    func retrievePhotoForPlace(place:Place, completion:ImageCompletion) {
        
        if let photoReference = place.photoReference {
            
            if let cachedImage = imageCache.imageWithIdentifier(photoReference) {
                
                completion(cachedImage)
                
            } else {
                
                let params = ["photoreference": "\(photoReference)", "maxwidth": "400", "key": Consts.GooglePlacesAPIKey.rawValue]
                
                Alamofire.request(.GET, Consts.GooglePlacesPhotoBaseUrl.rawValue, parameters:params)
                    .responseImage { response in
                        
                        if let image = response.result.value {
                            
                            self.imageCache.addImage(image, withIdentifier: photoReference)
                            
                            completion(image)
                            
                        }
                        
                }
                
            }
            
        }
        
    }
    
    func parsePlacesResponse(array:[AnyObject], completion:ArrayCompletion) {
        
        var places = [Place]()
                
        for placeDictionary in array {
            
            if let _ = placeDictionary as? [String:AnyObject] {
                
                let place = Place()
                
                if let id = placeDictionary["place_id"] as? String {
                    
                    place.id = id
                    
                }
                
                if let photos = placeDictionary["photos"] as? [[String:AnyObject]],
                    let photoDictionary = photos.first,
                    let photoReference = photoDictionary["photo_reference"] as? String {
                        
                        place.photoReference = photoReference
                    
                }
                
                if let name = placeDictionary["name"] as? String {
                    
                    place.name = name
                    
                }
                
                if let rating = placeDictionary["rating"] as? Float {
                    
                    place.rating = rating
                    
                }
                
                places.append(place)
                
                retrievePhotoForPlace(place, completion: { image in })
                
            }
            
        }
        
        if places.count > 0 {
            
            completion(places)
            
        }
    
    }
    
}