
import Foundation
import UIKit

class GameCell : UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    
    var index: Int = -1
    
    var place: Place?
    
    var image: UIImage? {
        
        didSet {
            
            self.imageView.image = self.image
            
        }
        
    }
    
    func setup() {
        
        NetworkManager.sharedInstance.retrievePhotoForPlace(place!) { image in
            
            self.image = image
            
        }
        
        if self.image == nil,
            let imageIndex = DataStore.sharedInstance.places.indexOf(place!) {
            
            image = UIImage(named: "image\(imageIndex)")
            
        }
        
        if DataStore.sharedInstance.isPlaceGuessed(place!) {
            
            backgroundColor = UIColor.greenColor()
            
            imageFadeIn(false)
            
        } else {
            
            backgroundColor = UIColor.grayColor()
            
        }
        
        if DataStore.sharedInstance.isCurrentlyTapped(self) {
            
            imageView.alpha = 1
            
        } else {
            
            imageView.alpha = 0
            
        }
    }
    
    func clear() {
        
        image = nil
        
        imageView.image = nil
        
        backgroundColor = UIColor.grayColor()
        
        index = -1
        
    }
    
    func imageFadeIn(fadeIn:Bool) {
        
        UIView.animateWithDuration(0.5, animations: {
        
            if fadeIn {
                
                self.imageView.alpha = 1
                
            } else {
                
                self.imageView.alpha = 0
                
            }
            
        })
        
    }
    
}