
protocol GameViewControllerDelegate : class {
    
    func updateAttempts()
    func gameSuccess()
    
}
