

import UIKit

class GameViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var informationLabel: UILabel!
    
    var delegate: GameViewControllerDelegate?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        refreshInformationLabel()
        
    }
    
    func loadGame() {
        
        DataStore.sharedInstance.allocateGamePlaces()
        
        refreshInformationLabel()
        
        self.collectionView.reloadData()
        
    }
    
    func refreshInformationLabel() {
        
        informationLabel.hidden = DataStore.sharedInstance.allocatedPlaces.count == 0 ? false : true
        
    }
    
    //MARK: UICollectionView methods
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return DataStore.sharedInstance.allocatedPlaces.count
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! GameCell
        
        cell.clear()
        
        cell.index = indexPath.row
        
        cell.place = DataStore.sharedInstance.allocatedPlaces[indexPath.row]
        
        cell.setup()
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! GameCell
        
        print("place name \(cell.place?.name)")
        
        let result = DataStore.sharedInstance.registerTapWithResult(cell)
        
        delegate?.updateAttempts()
        
        if result {
            
            delegate?.gameSuccess()
            
            DataStore.sharedInstance.clearCurrentGame()
                        
            collectionView.reloadData()
            
            refreshInformationLabel()
            
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let width = view.frame.width
        
        let cellWidth = width / 4 - 1
        
        let cellHeight = cellWidth
        
        let cellSize = CGSizeMake(cellWidth, cellHeight)
        
        return cellSize
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        return 1
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        return 1
        
    }
    
}

