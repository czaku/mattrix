
enum Consts : String {
    
    case GooglePlacesAPIKey = "AIzaSyCT-Aud2BGCI9QAbJlHA0gfBOMY7pIcXuw"
    case GooglePlacesLocationBaseUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
    case GooglePlacesPhotoBaseUrl = "https://maps.googleapis.com/maps/api/place/photo"

    case fallbackLatitude = "51.1106122"
    case fallbackLongitude = "17.0299246"
    
    case UserDefaultsBestScore = "UserDefaultsBestScore"
    
}