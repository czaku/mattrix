
import UIKit

typealias ArrayCompletion = ([AnyObject]) -> ()
typealias ImageCompletion = (UIImage) -> ()